#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>


/*--------------------------------------------------------------------------*/
/*                                                                          */
/*                              Quantisation                                */
/*                                                                          */
/*         (Copyright Irena Gali/c', 10/2008)                   */
/*                                                                          */
/*--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/

void alloc_matrix

     (float ***matrix,  /* matrix */
      long  nx,         /* size in x direction */
      long  ny)         /* size in y direction */

     /* allocates storage for matrix of size nx * ny */

{
long i;

*matrix = (float **) malloc (nx * sizeof(float *));
if (*matrix == NULL)
   {
   printf("alloc_matrix: not enough storage available\n");
   exit(1);
   }
for (i=0; i<nx; i++)
    {
    (*matrix)[i] = (float *) malloc (ny * sizeof(float));
    if ((*matrix)[i] == NULL)
       {
       printf("alloc_matrix: not enough storage available\n");
       exit(1);
       }
    }
return;
}

/*--------------------------------------------------------------------------*/

void disalloc_matrix

     (float **matrix,   /* matrix */
      long  nx,         /* size in x direction */
      long  ny)         /* size in y direction */

     /* disallocates storage for matrix of size nx * ny */

{
long i;
for (i=0; i<nx; i++)
    free(matrix[i]);
free(matrix);
return;
}

/*--------------------------------------------------------------------------*/

int main ()

{
char   row[80];              /* for reading data */
char   in[80];               /* for reading data */
char   out[80];              /* for reading data */
float  **u;                  /* image */
long   i, j;                 /* loop variables */ 
long   nx, ny;               /* image size in x, y direction */ 
FILE   *inimage, *outimage;  /* input file, output file */
int    q;                    /* number of bits used to represent a value
                                in the output image */
unsigned char byte;          /* for data conversion */

printf("\n");
printf("QUANTISATION\n\n");
printf("****************************************************************\n");
printf("\n");
printf("    Copyright 2009 by Markus Mainberger and Joachim Weickert\n");
printf("    Faculty of Mathematics and Computer Science\n");
printf("    Saarland University, Germany\n");
printf("\n");
printf("    All rights reserved. Unauthorized usage,\n");
printf("    copying, hiring, and selling prohibited.\n");
printf("\n");
printf("    Send bug reports to\n");
printf("    mainberger@mia.uni-saarland.de\n");
printf("\n");
printf("****************************************************************\n\n");

/* ---- read input image (pgm format P5) ---- */

/* read image name */
printf("input image:                                            ");
gets (in);

/* open pgm file and read header */
inimage = fopen(in,"r");
fgets (row, 80, inimage);
fgets (row, 80, inimage);
while (row[0]=='#') fgets(row, 80, inimage);
sscanf (row, "%ld %ld", &nx, &ny);
fgets (row, 80, inimage);

/* allocate storage */
alloc_matrix (&u, nx+2, ny+2);

/* read image data */
for (j=1; j<=ny; j++)
 for (i=1; i<=nx; i++)
     u[i][j] = (float) getc (inimage);
fclose(inimage);

/* ---- read other parameters ---- */

printf("number of bits used to represent a quantised value (q): ");
gets(row);  sscanf(row, "%i", &q);
printf("output image:                                           ");
gets(out);
printf("\n");


/* ---- process image ---- */

float d = powf(2.0f,8-q);
for (j=1; j<=ny; j++)
 for (i=1; i<=nx; i++)
 {
/*
 SUPPLEMENT MISSING CODE HERE
*/
 }

/* ---- write output image (pgm format P5) ---- */

/* open file and write header (incl. filter parameters) */
outimage = fopen (out, "w");
fprintf (outimage, "P5 \n");
fprintf (outimage, "# Quantisation\n");
fprintf (outimage, "# initial image:  %s\n", in);
fprintf (outimage, "# q:              %i\n", q);
fprintf (outimage, "%ld %ld \n255\n", nx, ny);

/* write image data and close file */
for (j=1; j<=ny; j++)
 for (i=1; i<=nx; i++)
     {
     if (u[i][j] < 0.0)
        byte = (unsigned char)(0.0);
     else if (u[i][j] > 255.0)
        byte = (unsigned char)(255.0);
     else
        byte = (unsigned char)(u[i][j]);
     fwrite (&byte, sizeof(unsigned char), 1, outimage);
     }
fclose(outimage);
printf("output image %s successfully written\n\n", out);


/* ---- disallocate storage ---- */

disalloc_matrix (u, nx+2, ny+2);

return(0);
}
